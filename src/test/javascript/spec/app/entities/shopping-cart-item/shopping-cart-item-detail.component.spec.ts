/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SupermercadoTestModule } from '../../../test.module';
import { ShoppingCartItemDetailComponent } from 'app/entities/shopping-cart-item/shopping-cart-item-detail.component';
import { ShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';

describe('Component Tests', () => {
  describe('ShoppingCartItem Management Detail Component', () => {
    let comp: ShoppingCartItemDetailComponent;
    let fixture: ComponentFixture<ShoppingCartItemDetailComponent>;
    const route = ({ data: of({ shoppingCartItem: new ShoppingCartItem(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SupermercadoTestModule],
        declarations: [ShoppingCartItemDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShoppingCartItemDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShoppingCartItemDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shoppingCartItem).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
