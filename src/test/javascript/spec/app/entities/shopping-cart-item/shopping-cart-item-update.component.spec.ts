/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { SupermercadoTestModule } from '../../../test.module';
import { ShoppingCartItemUpdateComponent } from 'app/entities/shopping-cart-item/shopping-cart-item-update.component';
import { ShoppingCartItemService } from 'app/entities/shopping-cart-item/shopping-cart-item.service';
import { ShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';

describe('Component Tests', () => {
  describe('ShoppingCartItem Management Update Component', () => {
    let comp: ShoppingCartItemUpdateComponent;
    let fixture: ComponentFixture<ShoppingCartItemUpdateComponent>;
    let service: ShoppingCartItemService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SupermercadoTestModule],
        declarations: [ShoppingCartItemUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShoppingCartItemUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShoppingCartItemUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingCartItemService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingCartItem(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingCartItem();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
