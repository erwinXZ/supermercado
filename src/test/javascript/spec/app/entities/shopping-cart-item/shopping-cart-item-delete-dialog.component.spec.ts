/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SupermercadoTestModule } from '../../../test.module';
import { ShoppingCartItemDeleteDialogComponent } from 'app/entities/shopping-cart-item/shopping-cart-item-delete-dialog.component';
import { ShoppingCartItemService } from 'app/entities/shopping-cart-item/shopping-cart-item.service';

describe('Component Tests', () => {
  describe('ShoppingCartItem Management Delete Component', () => {
    let comp: ShoppingCartItemDeleteDialogComponent;
    let fixture: ComponentFixture<ShoppingCartItemDeleteDialogComponent>;
    let service: ShoppingCartItemService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SupermercadoTestModule],
        declarations: [ShoppingCartItemDeleteDialogComponent]
      })
        .overrideTemplate(ShoppingCartItemDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShoppingCartItemDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingCartItemService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
