package com.itgroup.supermercado.web.rest;

import com.itgroup.supermercado.SupermercadoApp;
import com.itgroup.supermercado.domain.ShoppingCartItem;
import com.itgroup.supermercado.domain.Product;
import com.itgroup.supermercado.domain.Sale;
import com.itgroup.supermercado.repository.ShoppingCartItemRepository;
import com.itgroup.supermercado.service.ShoppingCartItemService;
import com.itgroup.supermercado.web.rest.errors.ExceptionTranslator;
import com.itgroup.supermercado.service.dto.ShoppingCartItemCriteria;
import com.itgroup.supermercado.service.ShoppingCartItemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static com.itgroup.supermercado.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShoppingCartItemResource} REST controller.
 */
@SpringBootTest(classes = SupermercadoApp.class)
public class ShoppingCartItemResourceIT {

    private static final BigDecimal DEFAULT_TOTAL = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL = new BigDecimal(2);
    private static final BigDecimal SMALLER_TOTAL = new BigDecimal(1 - 1);

    private static final Integer DEFAULT_PRODUCT_QUANTITY = 1;
    private static final Integer UPDATED_PRODUCT_QUANTITY = 2;
    private static final Integer SMALLER_PRODUCT_QUANTITY = 1 - 1;

    @Autowired
    private ShoppingCartItemRepository shoppingCartItemRepository;

    @Autowired
    private ShoppingCartItemService shoppingCartItemService;

    @Autowired
    private ShoppingCartItemQueryService shoppingCartItemQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShoppingCartItemMockMvc;

    private ShoppingCartItem shoppingCartItem;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShoppingCartItemResource shoppingCartItemResource = new ShoppingCartItemResource(shoppingCartItemService, shoppingCartItemQueryService);
        this.restShoppingCartItemMockMvc = MockMvcBuilders.standaloneSetup(shoppingCartItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingCartItem createEntity(EntityManager em) {
        ShoppingCartItem shoppingCartItem = new ShoppingCartItem()
            .total(DEFAULT_TOTAL)
            .productQuantity(DEFAULT_PRODUCT_QUANTITY);
        return shoppingCartItem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingCartItem createUpdatedEntity(EntityManager em) {
        ShoppingCartItem shoppingCartItem = new ShoppingCartItem()
            .total(UPDATED_TOTAL)
            .productQuantity(UPDATED_PRODUCT_QUANTITY);
        return shoppingCartItem;
    }

    @BeforeEach
    public void initTest() {
        shoppingCartItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createShoppingCartItem() throws Exception {
        int databaseSizeBeforeCreate = shoppingCartItemRepository.findAll().size();

        // Create the ShoppingCartItem
        restShoppingCartItemMockMvc.perform(post("/api/shopping-cart-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCartItem)))
            .andExpect(status().isCreated());

        // Validate the ShoppingCartItem in the database
        List<ShoppingCartItem> shoppingCartItemList = shoppingCartItemRepository.findAll();
        assertThat(shoppingCartItemList).hasSize(databaseSizeBeforeCreate + 1);
        ShoppingCartItem testShoppingCartItem = shoppingCartItemList.get(shoppingCartItemList.size() - 1);
        assertThat(testShoppingCartItem.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testShoppingCartItem.getProductQuantity()).isEqualTo(DEFAULT_PRODUCT_QUANTITY);
    }

    @Test
    @Transactional
    public void createShoppingCartItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shoppingCartItemRepository.findAll().size();

        // Create the ShoppingCartItem with an existing ID
        shoppingCartItem.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShoppingCartItemMockMvc.perform(post("/api/shopping-cart-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCartItem)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingCartItem in the database
        List<ShoppingCartItem> shoppingCartItemList = shoppingCartItemRepository.findAll();
        assertThat(shoppingCartItemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllShoppingCartItems() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList
        restShoppingCartItemMockMvc.perform(get("/api/shopping-cart-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingCartItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.intValue())))
            .andExpect(jsonPath("$.[*].productQuantity").value(hasItem(DEFAULT_PRODUCT_QUANTITY)));
    }
    
    @Test
    @Transactional
    public void getShoppingCartItem() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get the shoppingCartItem
        restShoppingCartItemMockMvc.perform(get("/api/shopping-cart-items/{id}", shoppingCartItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shoppingCartItem.getId().intValue()))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL.intValue()))
            .andExpect(jsonPath("$.productQuantity").value(DEFAULT_PRODUCT_QUANTITY));
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByTotalIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where total equals to DEFAULT_TOTAL
        defaultShoppingCartItemShouldBeFound("total.equals=" + DEFAULT_TOTAL);

        // Get all the shoppingCartItemList where total equals to UPDATED_TOTAL
        defaultShoppingCartItemShouldNotBeFound("total.equals=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByTotalIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where total in DEFAULT_TOTAL or UPDATED_TOTAL
        defaultShoppingCartItemShouldBeFound("total.in=" + DEFAULT_TOTAL + "," + UPDATED_TOTAL);

        // Get all the shoppingCartItemList where total equals to UPDATED_TOTAL
        defaultShoppingCartItemShouldNotBeFound("total.in=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByTotalIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where total is not null
        defaultShoppingCartItemShouldBeFound("total.specified=true");

        // Get all the shoppingCartItemList where total is null
        defaultShoppingCartItemShouldNotBeFound("total.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByTotalIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where total is greater than or equal to DEFAULT_TOTAL
        defaultShoppingCartItemShouldBeFound("total.greaterThanOrEqual=" + DEFAULT_TOTAL);

        // Get all the shoppingCartItemList where total is greater than or equal to UPDATED_TOTAL
        defaultShoppingCartItemShouldNotBeFound("total.greaterThanOrEqual=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByTotalIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where total is less than or equal to DEFAULT_TOTAL
        defaultShoppingCartItemShouldBeFound("total.lessThanOrEqual=" + DEFAULT_TOTAL);

        // Get all the shoppingCartItemList where total is less than or equal to SMALLER_TOTAL
        defaultShoppingCartItemShouldNotBeFound("total.lessThanOrEqual=" + SMALLER_TOTAL);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByTotalIsLessThanSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where total is less than DEFAULT_TOTAL
        defaultShoppingCartItemShouldNotBeFound("total.lessThan=" + DEFAULT_TOTAL);

        // Get all the shoppingCartItemList where total is less than UPDATED_TOTAL
        defaultShoppingCartItemShouldBeFound("total.lessThan=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByTotalIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where total is greater than DEFAULT_TOTAL
        defaultShoppingCartItemShouldNotBeFound("total.greaterThan=" + DEFAULT_TOTAL);

        // Get all the shoppingCartItemList where total is greater than SMALLER_TOTAL
        defaultShoppingCartItemShouldBeFound("total.greaterThan=" + SMALLER_TOTAL);
    }


    @Test
    @Transactional
    public void getAllShoppingCartItemsByProductQuantityIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where productQuantity equals to DEFAULT_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldBeFound("productQuantity.equals=" + DEFAULT_PRODUCT_QUANTITY);

        // Get all the shoppingCartItemList where productQuantity equals to UPDATED_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldNotBeFound("productQuantity.equals=" + UPDATED_PRODUCT_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByProductQuantityIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where productQuantity in DEFAULT_PRODUCT_QUANTITY or UPDATED_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldBeFound("productQuantity.in=" + DEFAULT_PRODUCT_QUANTITY + "," + UPDATED_PRODUCT_QUANTITY);

        // Get all the shoppingCartItemList where productQuantity equals to UPDATED_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldNotBeFound("productQuantity.in=" + UPDATED_PRODUCT_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByProductQuantityIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where productQuantity is not null
        defaultShoppingCartItemShouldBeFound("productQuantity.specified=true");

        // Get all the shoppingCartItemList where productQuantity is null
        defaultShoppingCartItemShouldNotBeFound("productQuantity.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByProductQuantityIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where productQuantity is greater than or equal to DEFAULT_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldBeFound("productQuantity.greaterThanOrEqual=" + DEFAULT_PRODUCT_QUANTITY);

        // Get all the shoppingCartItemList where productQuantity is greater than or equal to UPDATED_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldNotBeFound("productQuantity.greaterThanOrEqual=" + UPDATED_PRODUCT_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByProductQuantityIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where productQuantity is less than or equal to DEFAULT_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldBeFound("productQuantity.lessThanOrEqual=" + DEFAULT_PRODUCT_QUANTITY);

        // Get all the shoppingCartItemList where productQuantity is less than or equal to SMALLER_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldNotBeFound("productQuantity.lessThanOrEqual=" + SMALLER_PRODUCT_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByProductQuantityIsLessThanSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where productQuantity is less than DEFAULT_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldNotBeFound("productQuantity.lessThan=" + DEFAULT_PRODUCT_QUANTITY);

        // Get all the shoppingCartItemList where productQuantity is less than UPDATED_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldBeFound("productQuantity.lessThan=" + UPDATED_PRODUCT_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllShoppingCartItemsByProductQuantityIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);

        // Get all the shoppingCartItemList where productQuantity is greater than DEFAULT_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldNotBeFound("productQuantity.greaterThan=" + DEFAULT_PRODUCT_QUANTITY);

        // Get all the shoppingCartItemList where productQuantity is greater than SMALLER_PRODUCT_QUANTITY
        defaultShoppingCartItemShouldBeFound("productQuantity.greaterThan=" + SMALLER_PRODUCT_QUANTITY);
    }


    @Test
    @Transactional
    public void getAllShoppingCartItemsByProductIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);
        Product product = ProductResourceIT.createEntity(em);
        em.persist(product);
        em.flush();
        shoppingCartItem.setProduct(product);
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);
        Long productId = product.getId();

        // Get all the shoppingCartItemList where product equals to productId
        defaultShoppingCartItemShouldBeFound("productId.equals=" + productId);

        // Get all the shoppingCartItemList where product equals to productId + 1
        defaultShoppingCartItemShouldNotBeFound("productId.equals=" + (productId + 1));
    }


    @Test
    @Transactional
    public void getAllShoppingCartItemsBySaleIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);
        Sale sale = SaleResourceIT.createEntity(em);
        em.persist(sale);
        em.flush();
        shoppingCartItem.setSale(sale);
        shoppingCartItemRepository.saveAndFlush(shoppingCartItem);
        Long saleId = sale.getId();

        // Get all the shoppingCartItemList where sale equals to saleId
        defaultShoppingCartItemShouldBeFound("saleId.equals=" + saleId);

        // Get all the shoppingCartItemList where sale equals to saleId + 1
        defaultShoppingCartItemShouldNotBeFound("saleId.equals=" + (saleId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultShoppingCartItemShouldBeFound(String filter) throws Exception {
        restShoppingCartItemMockMvc.perform(get("/api/shopping-cart-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingCartItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.intValue())))
            .andExpect(jsonPath("$.[*].productQuantity").value(hasItem(DEFAULT_PRODUCT_QUANTITY)));

        // Check, that the count call also returns 1
        restShoppingCartItemMockMvc.perform(get("/api/shopping-cart-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultShoppingCartItemShouldNotBeFound(String filter) throws Exception {
        restShoppingCartItemMockMvc.perform(get("/api/shopping-cart-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restShoppingCartItemMockMvc.perform(get("/api/shopping-cart-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingShoppingCartItem() throws Exception {
        // Get the shoppingCartItem
        restShoppingCartItemMockMvc.perform(get("/api/shopping-cart-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShoppingCartItem() throws Exception {
        // Initialize the database
        shoppingCartItemService.save(shoppingCartItem);

        int databaseSizeBeforeUpdate = shoppingCartItemRepository.findAll().size();

        // Update the shoppingCartItem
        ShoppingCartItem updatedShoppingCartItem = shoppingCartItemRepository.findById(shoppingCartItem.getId()).get();
        // Disconnect from session so that the updates on updatedShoppingCartItem are not directly saved in db
        em.detach(updatedShoppingCartItem);
        updatedShoppingCartItem
            .total(UPDATED_TOTAL)
            .productQuantity(UPDATED_PRODUCT_QUANTITY);

        restShoppingCartItemMockMvc.perform(put("/api/shopping-cart-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedShoppingCartItem)))
            .andExpect(status().isOk());

        // Validate the ShoppingCartItem in the database
        List<ShoppingCartItem> shoppingCartItemList = shoppingCartItemRepository.findAll();
        assertThat(shoppingCartItemList).hasSize(databaseSizeBeforeUpdate);
        ShoppingCartItem testShoppingCartItem = shoppingCartItemList.get(shoppingCartItemList.size() - 1);
        assertThat(testShoppingCartItem.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testShoppingCartItem.getProductQuantity()).isEqualTo(UPDATED_PRODUCT_QUANTITY);
    }

    @Test
    @Transactional
    public void updateNonExistingShoppingCartItem() throws Exception {
        int databaseSizeBeforeUpdate = shoppingCartItemRepository.findAll().size();

        // Create the ShoppingCartItem

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShoppingCartItemMockMvc.perform(put("/api/shopping-cart-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoppingCartItem)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingCartItem in the database
        List<ShoppingCartItem> shoppingCartItemList = shoppingCartItemRepository.findAll();
        assertThat(shoppingCartItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShoppingCartItem() throws Exception {
        // Initialize the database
        shoppingCartItemService.save(shoppingCartItem);

        int databaseSizeBeforeDelete = shoppingCartItemRepository.findAll().size();

        // Delete the shoppingCartItem
        restShoppingCartItemMockMvc.perform(delete("/api/shopping-cart-items/{id}", shoppingCartItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShoppingCartItem> shoppingCartItemList = shoppingCartItemRepository.findAll();
        assertThat(shoppingCartItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingCartItem.class);
        ShoppingCartItem shoppingCartItem1 = new ShoppingCartItem();
        shoppingCartItem1.setId(1L);
        ShoppingCartItem shoppingCartItem2 = new ShoppingCartItem();
        shoppingCartItem2.setId(shoppingCartItem1.getId());
        assertThat(shoppingCartItem1).isEqualTo(shoppingCartItem2);
        shoppingCartItem2.setId(2L);
        assertThat(shoppingCartItem1).isNotEqualTo(shoppingCartItem2);
        shoppingCartItem1.setId(null);
        assertThat(shoppingCartItem1).isNotEqualTo(shoppingCartItem2);
    }
}
