package com.itgroup.supermercado.web.rest;

import com.itgroup.supermercado.SupermercadoApp;
import com.itgroup.supermercado.domain.Sale;
import com.itgroup.supermercado.domain.ShoppingCartItem;
import com.itgroup.supermercado.domain.User;
import com.itgroup.supermercado.repository.SaleRepository;
import com.itgroup.supermercado.service.SaleService;
import com.itgroup.supermercado.web.rest.errors.ExceptionTranslator;
import com.itgroup.supermercado.service.dto.SaleCriteria;
import com.itgroup.supermercado.service.SaleQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static com.itgroup.supermercado.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SaleResource} REST controller.
 */
@SpringBootTest(classes = SupermercadoApp.class)
public class SaleResourceIT {

    private static final BigDecimal DEFAULT_SUBTOTAL = new BigDecimal(1);
    private static final BigDecimal UPDATED_SUBTOTAL = new BigDecimal(2);
    private static final BigDecimal SMALLER_SUBTOTAL = new BigDecimal(1 - 1);

    private static final Integer DEFAULT_PRODUCTS_QUANTITY = 1;
    private static final Integer UPDATED_PRODUCTS_QUANTITY = 2;
    private static final Integer SMALLER_PRODUCTS_QUANTITY = 1 - 1;

    private static final BigDecimal DEFAULT_DISCOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DISCOUNT = new BigDecimal(2);
    private static final BigDecimal SMALLER_DISCOUNT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_IVA = new BigDecimal(1);
    private static final BigDecimal UPDATED_IVA = new BigDecimal(2);
    private static final BigDecimal SMALLER_IVA = new BigDecimal(1 - 1);

    private static final String DEFAULT_INVOICE_BUSINESS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_INVOICE_BUSINESS_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NIT_DOCUMENT = "AAAAAAAAAA";
    private static final String UPDATED_NIT_DOCUMENT = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TOTAL = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL = new BigDecimal(2);
    private static final BigDecimal SMALLER_TOTAL = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_TOTAL_PAID = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_PAID = new BigDecimal(2);
    private static final BigDecimal SMALLER_TOTAL_PAID = new BigDecimal(1 - 1);

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private SaleService saleService;

    @Autowired
    private SaleQueryService saleQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSaleMockMvc;

    private Sale sale;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SaleResource saleResource = new SaleResource(saleService, saleQueryService);
        this.restSaleMockMvc = MockMvcBuilders.standaloneSetup(saleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sale createEntity(EntityManager em) {
        Sale sale = new Sale()
            .subtotal(DEFAULT_SUBTOTAL)
            .productsQuantity(DEFAULT_PRODUCTS_QUANTITY)
            .discount(DEFAULT_DISCOUNT)
            .iva(DEFAULT_IVA)
            .invoiceBusinessName(DEFAULT_INVOICE_BUSINESS_NAME)
            .nitDocument(DEFAULT_NIT_DOCUMENT)
            .total(DEFAULT_TOTAL)
            .totalPaid(DEFAULT_TOTAL_PAID);
        return sale;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sale createUpdatedEntity(EntityManager em) {
        Sale sale = new Sale()
            .subtotal(UPDATED_SUBTOTAL)
            .productsQuantity(UPDATED_PRODUCTS_QUANTITY)
            .discount(UPDATED_DISCOUNT)
            .iva(UPDATED_IVA)
            .invoiceBusinessName(UPDATED_INVOICE_BUSINESS_NAME)
            .nitDocument(UPDATED_NIT_DOCUMENT)
            .total(UPDATED_TOTAL)
            .totalPaid(UPDATED_TOTAL_PAID);
        return sale;
    }

    @BeforeEach
    public void initTest() {
        sale = createEntity(em);
    }

    @Test
    @Transactional
    public void createSale() throws Exception {
        int databaseSizeBeforeCreate = saleRepository.findAll().size();

        // Create the Sale
        restSaleMockMvc.perform(post("/api/sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sale)))
            .andExpect(status().isCreated());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeCreate + 1);
        Sale testSale = saleList.get(saleList.size() - 1);
        assertThat(testSale.getSubtotal()).isEqualTo(DEFAULT_SUBTOTAL);
        assertThat(testSale.getProductsQuantity()).isEqualTo(DEFAULT_PRODUCTS_QUANTITY);
        assertThat(testSale.getDiscount()).isEqualTo(DEFAULT_DISCOUNT);
        assertThat(testSale.getIva()).isEqualTo(DEFAULT_IVA);
        assertThat(testSale.getInvoiceBusinessName()).isEqualTo(DEFAULT_INVOICE_BUSINESS_NAME);
        assertThat(testSale.getNitDocument()).isEqualTo(DEFAULT_NIT_DOCUMENT);
        assertThat(testSale.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testSale.getTotalPaid()).isEqualTo(DEFAULT_TOTAL_PAID);
    }

    @Test
    @Transactional
    public void createSaleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = saleRepository.findAll().size();

        // Create the Sale with an existing ID
        sale.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSaleMockMvc.perform(post("/api/sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sale)))
            .andExpect(status().isBadRequest());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSales() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList
        restSaleMockMvc.perform(get("/api/sales?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sale.getId().intValue())))
            .andExpect(jsonPath("$.[*].subtotal").value(hasItem(DEFAULT_SUBTOTAL.intValue())))
            .andExpect(jsonPath("$.[*].productsQuantity").value(hasItem(DEFAULT_PRODUCTS_QUANTITY)))
            .andExpect(jsonPath("$.[*].discount").value(hasItem(DEFAULT_DISCOUNT.intValue())))
            .andExpect(jsonPath("$.[*].iva").value(hasItem(DEFAULT_IVA.intValue())))
            .andExpect(jsonPath("$.[*].invoiceBusinessName").value(hasItem(DEFAULT_INVOICE_BUSINESS_NAME.toString())))
            .andExpect(jsonPath("$.[*].nitDocument").value(hasItem(DEFAULT_NIT_DOCUMENT.toString())))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.intValue())))
            .andExpect(jsonPath("$.[*].totalPaid").value(hasItem(DEFAULT_TOTAL_PAID.intValue())));
    }
    
    @Test
    @Transactional
    public void getSale() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get the sale
        restSaleMockMvc.perform(get("/api/sales/{id}", sale.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sale.getId().intValue()))
            .andExpect(jsonPath("$.subtotal").value(DEFAULT_SUBTOTAL.intValue()))
            .andExpect(jsonPath("$.productsQuantity").value(DEFAULT_PRODUCTS_QUANTITY))
            .andExpect(jsonPath("$.discount").value(DEFAULT_DISCOUNT.intValue()))
            .andExpect(jsonPath("$.iva").value(DEFAULT_IVA.intValue()))
            .andExpect(jsonPath("$.invoiceBusinessName").value(DEFAULT_INVOICE_BUSINESS_NAME.toString()))
            .andExpect(jsonPath("$.nitDocument").value(DEFAULT_NIT_DOCUMENT.toString()))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL.intValue()))
            .andExpect(jsonPath("$.totalPaid").value(DEFAULT_TOTAL_PAID.intValue()));
    }

    @Test
    @Transactional
    public void getAllSalesBySubtotalIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where subtotal equals to DEFAULT_SUBTOTAL
        defaultSaleShouldBeFound("subtotal.equals=" + DEFAULT_SUBTOTAL);

        // Get all the saleList where subtotal equals to UPDATED_SUBTOTAL
        defaultSaleShouldNotBeFound("subtotal.equals=" + UPDATED_SUBTOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesBySubtotalIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where subtotal in DEFAULT_SUBTOTAL or UPDATED_SUBTOTAL
        defaultSaleShouldBeFound("subtotal.in=" + DEFAULT_SUBTOTAL + "," + UPDATED_SUBTOTAL);

        // Get all the saleList where subtotal equals to UPDATED_SUBTOTAL
        defaultSaleShouldNotBeFound("subtotal.in=" + UPDATED_SUBTOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesBySubtotalIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where subtotal is not null
        defaultSaleShouldBeFound("subtotal.specified=true");

        // Get all the saleList where subtotal is null
        defaultSaleShouldNotBeFound("subtotal.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesBySubtotalIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where subtotal is greater than or equal to DEFAULT_SUBTOTAL
        defaultSaleShouldBeFound("subtotal.greaterThanOrEqual=" + DEFAULT_SUBTOTAL);

        // Get all the saleList where subtotal is greater than or equal to UPDATED_SUBTOTAL
        defaultSaleShouldNotBeFound("subtotal.greaterThanOrEqual=" + UPDATED_SUBTOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesBySubtotalIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where subtotal is less than or equal to DEFAULT_SUBTOTAL
        defaultSaleShouldBeFound("subtotal.lessThanOrEqual=" + DEFAULT_SUBTOTAL);

        // Get all the saleList where subtotal is less than or equal to SMALLER_SUBTOTAL
        defaultSaleShouldNotBeFound("subtotal.lessThanOrEqual=" + SMALLER_SUBTOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesBySubtotalIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where subtotal is less than DEFAULT_SUBTOTAL
        defaultSaleShouldNotBeFound("subtotal.lessThan=" + DEFAULT_SUBTOTAL);

        // Get all the saleList where subtotal is less than UPDATED_SUBTOTAL
        defaultSaleShouldBeFound("subtotal.lessThan=" + UPDATED_SUBTOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesBySubtotalIsGreaterThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where subtotal is greater than DEFAULT_SUBTOTAL
        defaultSaleShouldNotBeFound("subtotal.greaterThan=" + DEFAULT_SUBTOTAL);

        // Get all the saleList where subtotal is greater than SMALLER_SUBTOTAL
        defaultSaleShouldBeFound("subtotal.greaterThan=" + SMALLER_SUBTOTAL);
    }


    @Test
    @Transactional
    public void getAllSalesByProductsQuantityIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where productsQuantity equals to DEFAULT_PRODUCTS_QUANTITY
        defaultSaleShouldBeFound("productsQuantity.equals=" + DEFAULT_PRODUCTS_QUANTITY);

        // Get all the saleList where productsQuantity equals to UPDATED_PRODUCTS_QUANTITY
        defaultSaleShouldNotBeFound("productsQuantity.equals=" + UPDATED_PRODUCTS_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllSalesByProductsQuantityIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where productsQuantity in DEFAULT_PRODUCTS_QUANTITY or UPDATED_PRODUCTS_QUANTITY
        defaultSaleShouldBeFound("productsQuantity.in=" + DEFAULT_PRODUCTS_QUANTITY + "," + UPDATED_PRODUCTS_QUANTITY);

        // Get all the saleList where productsQuantity equals to UPDATED_PRODUCTS_QUANTITY
        defaultSaleShouldNotBeFound("productsQuantity.in=" + UPDATED_PRODUCTS_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllSalesByProductsQuantityIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where productsQuantity is not null
        defaultSaleShouldBeFound("productsQuantity.specified=true");

        // Get all the saleList where productsQuantity is null
        defaultSaleShouldNotBeFound("productsQuantity.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByProductsQuantityIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where productsQuantity is greater than or equal to DEFAULT_PRODUCTS_QUANTITY
        defaultSaleShouldBeFound("productsQuantity.greaterThanOrEqual=" + DEFAULT_PRODUCTS_QUANTITY);

        // Get all the saleList where productsQuantity is greater than or equal to UPDATED_PRODUCTS_QUANTITY
        defaultSaleShouldNotBeFound("productsQuantity.greaterThanOrEqual=" + UPDATED_PRODUCTS_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllSalesByProductsQuantityIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where productsQuantity is less than or equal to DEFAULT_PRODUCTS_QUANTITY
        defaultSaleShouldBeFound("productsQuantity.lessThanOrEqual=" + DEFAULT_PRODUCTS_QUANTITY);

        // Get all the saleList where productsQuantity is less than or equal to SMALLER_PRODUCTS_QUANTITY
        defaultSaleShouldNotBeFound("productsQuantity.lessThanOrEqual=" + SMALLER_PRODUCTS_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllSalesByProductsQuantityIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where productsQuantity is less than DEFAULT_PRODUCTS_QUANTITY
        defaultSaleShouldNotBeFound("productsQuantity.lessThan=" + DEFAULT_PRODUCTS_QUANTITY);

        // Get all the saleList where productsQuantity is less than UPDATED_PRODUCTS_QUANTITY
        defaultSaleShouldBeFound("productsQuantity.lessThan=" + UPDATED_PRODUCTS_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllSalesByProductsQuantityIsGreaterThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where productsQuantity is greater than DEFAULT_PRODUCTS_QUANTITY
        defaultSaleShouldNotBeFound("productsQuantity.greaterThan=" + DEFAULT_PRODUCTS_QUANTITY);

        // Get all the saleList where productsQuantity is greater than SMALLER_PRODUCTS_QUANTITY
        defaultSaleShouldBeFound("productsQuantity.greaterThan=" + SMALLER_PRODUCTS_QUANTITY);
    }


    @Test
    @Transactional
    public void getAllSalesByDiscountIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where discount equals to DEFAULT_DISCOUNT
        defaultSaleShouldBeFound("discount.equals=" + DEFAULT_DISCOUNT);

        // Get all the saleList where discount equals to UPDATED_DISCOUNT
        defaultSaleShouldNotBeFound("discount.equals=" + UPDATED_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllSalesByDiscountIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where discount in DEFAULT_DISCOUNT or UPDATED_DISCOUNT
        defaultSaleShouldBeFound("discount.in=" + DEFAULT_DISCOUNT + "," + UPDATED_DISCOUNT);

        // Get all the saleList where discount equals to UPDATED_DISCOUNT
        defaultSaleShouldNotBeFound("discount.in=" + UPDATED_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllSalesByDiscountIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where discount is not null
        defaultSaleShouldBeFound("discount.specified=true");

        // Get all the saleList where discount is null
        defaultSaleShouldNotBeFound("discount.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByDiscountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where discount is greater than or equal to DEFAULT_DISCOUNT
        defaultSaleShouldBeFound("discount.greaterThanOrEqual=" + DEFAULT_DISCOUNT);

        // Get all the saleList where discount is greater than or equal to UPDATED_DISCOUNT
        defaultSaleShouldNotBeFound("discount.greaterThanOrEqual=" + UPDATED_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllSalesByDiscountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where discount is less than or equal to DEFAULT_DISCOUNT
        defaultSaleShouldBeFound("discount.lessThanOrEqual=" + DEFAULT_DISCOUNT);

        // Get all the saleList where discount is less than or equal to SMALLER_DISCOUNT
        defaultSaleShouldNotBeFound("discount.lessThanOrEqual=" + SMALLER_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllSalesByDiscountIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where discount is less than DEFAULT_DISCOUNT
        defaultSaleShouldNotBeFound("discount.lessThan=" + DEFAULT_DISCOUNT);

        // Get all the saleList where discount is less than UPDATED_DISCOUNT
        defaultSaleShouldBeFound("discount.lessThan=" + UPDATED_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllSalesByDiscountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where discount is greater than DEFAULT_DISCOUNT
        defaultSaleShouldNotBeFound("discount.greaterThan=" + DEFAULT_DISCOUNT);

        // Get all the saleList where discount is greater than SMALLER_DISCOUNT
        defaultSaleShouldBeFound("discount.greaterThan=" + SMALLER_DISCOUNT);
    }


    @Test
    @Transactional
    public void getAllSalesByIvaIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where iva equals to DEFAULT_IVA
        defaultSaleShouldBeFound("iva.equals=" + DEFAULT_IVA);

        // Get all the saleList where iva equals to UPDATED_IVA
        defaultSaleShouldNotBeFound("iva.equals=" + UPDATED_IVA);
    }

    @Test
    @Transactional
    public void getAllSalesByIvaIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where iva in DEFAULT_IVA or UPDATED_IVA
        defaultSaleShouldBeFound("iva.in=" + DEFAULT_IVA + "," + UPDATED_IVA);

        // Get all the saleList where iva equals to UPDATED_IVA
        defaultSaleShouldNotBeFound("iva.in=" + UPDATED_IVA);
    }

    @Test
    @Transactional
    public void getAllSalesByIvaIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where iva is not null
        defaultSaleShouldBeFound("iva.specified=true");

        // Get all the saleList where iva is null
        defaultSaleShouldNotBeFound("iva.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByIvaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where iva is greater than or equal to DEFAULT_IVA
        defaultSaleShouldBeFound("iva.greaterThanOrEqual=" + DEFAULT_IVA);

        // Get all the saleList where iva is greater than or equal to UPDATED_IVA
        defaultSaleShouldNotBeFound("iva.greaterThanOrEqual=" + UPDATED_IVA);
    }

    @Test
    @Transactional
    public void getAllSalesByIvaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where iva is less than or equal to DEFAULT_IVA
        defaultSaleShouldBeFound("iva.lessThanOrEqual=" + DEFAULT_IVA);

        // Get all the saleList where iva is less than or equal to SMALLER_IVA
        defaultSaleShouldNotBeFound("iva.lessThanOrEqual=" + SMALLER_IVA);
    }

    @Test
    @Transactional
    public void getAllSalesByIvaIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where iva is less than DEFAULT_IVA
        defaultSaleShouldNotBeFound("iva.lessThan=" + DEFAULT_IVA);

        // Get all the saleList where iva is less than UPDATED_IVA
        defaultSaleShouldBeFound("iva.lessThan=" + UPDATED_IVA);
    }

    @Test
    @Transactional
    public void getAllSalesByIvaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where iva is greater than DEFAULT_IVA
        defaultSaleShouldNotBeFound("iva.greaterThan=" + DEFAULT_IVA);

        // Get all the saleList where iva is greater than SMALLER_IVA
        defaultSaleShouldBeFound("iva.greaterThan=" + SMALLER_IVA);
    }


    @Test
    @Transactional
    public void getAllSalesByInvoiceBusinessNameIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where invoiceBusinessName equals to DEFAULT_INVOICE_BUSINESS_NAME
        defaultSaleShouldBeFound("invoiceBusinessName.equals=" + DEFAULT_INVOICE_BUSINESS_NAME);

        // Get all the saleList where invoiceBusinessName equals to UPDATED_INVOICE_BUSINESS_NAME
        defaultSaleShouldNotBeFound("invoiceBusinessName.equals=" + UPDATED_INVOICE_BUSINESS_NAME);
    }

    @Test
    @Transactional
    public void getAllSalesByInvoiceBusinessNameIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where invoiceBusinessName in DEFAULT_INVOICE_BUSINESS_NAME or UPDATED_INVOICE_BUSINESS_NAME
        defaultSaleShouldBeFound("invoiceBusinessName.in=" + DEFAULT_INVOICE_BUSINESS_NAME + "," + UPDATED_INVOICE_BUSINESS_NAME);

        // Get all the saleList where invoiceBusinessName equals to UPDATED_INVOICE_BUSINESS_NAME
        defaultSaleShouldNotBeFound("invoiceBusinessName.in=" + UPDATED_INVOICE_BUSINESS_NAME);
    }

    @Test
    @Transactional
    public void getAllSalesByInvoiceBusinessNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where invoiceBusinessName is not null
        defaultSaleShouldBeFound("invoiceBusinessName.specified=true");

        // Get all the saleList where invoiceBusinessName is null
        defaultSaleShouldNotBeFound("invoiceBusinessName.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByNitDocumentIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where nitDocument equals to DEFAULT_NIT_DOCUMENT
        defaultSaleShouldBeFound("nitDocument.equals=" + DEFAULT_NIT_DOCUMENT);

        // Get all the saleList where nitDocument equals to UPDATED_NIT_DOCUMENT
        defaultSaleShouldNotBeFound("nitDocument.equals=" + UPDATED_NIT_DOCUMENT);
    }

    @Test
    @Transactional
    public void getAllSalesByNitDocumentIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where nitDocument in DEFAULT_NIT_DOCUMENT or UPDATED_NIT_DOCUMENT
        defaultSaleShouldBeFound("nitDocument.in=" + DEFAULT_NIT_DOCUMENT + "," + UPDATED_NIT_DOCUMENT);

        // Get all the saleList where nitDocument equals to UPDATED_NIT_DOCUMENT
        defaultSaleShouldNotBeFound("nitDocument.in=" + UPDATED_NIT_DOCUMENT);
    }

    @Test
    @Transactional
    public void getAllSalesByNitDocumentIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where nitDocument is not null
        defaultSaleShouldBeFound("nitDocument.specified=true");

        // Get all the saleList where nitDocument is null
        defaultSaleShouldNotBeFound("nitDocument.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByTotalIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where total equals to DEFAULT_TOTAL
        defaultSaleShouldBeFound("total.equals=" + DEFAULT_TOTAL);

        // Get all the saleList where total equals to UPDATED_TOTAL
        defaultSaleShouldNotBeFound("total.equals=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where total in DEFAULT_TOTAL or UPDATED_TOTAL
        defaultSaleShouldBeFound("total.in=" + DEFAULT_TOTAL + "," + UPDATED_TOTAL);

        // Get all the saleList where total equals to UPDATED_TOTAL
        defaultSaleShouldNotBeFound("total.in=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where total is not null
        defaultSaleShouldBeFound("total.specified=true");

        // Get all the saleList where total is null
        defaultSaleShouldNotBeFound("total.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByTotalIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where total is greater than or equal to DEFAULT_TOTAL
        defaultSaleShouldBeFound("total.greaterThanOrEqual=" + DEFAULT_TOTAL);

        // Get all the saleList where total is greater than or equal to UPDATED_TOTAL
        defaultSaleShouldNotBeFound("total.greaterThanOrEqual=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where total is less than or equal to DEFAULT_TOTAL
        defaultSaleShouldBeFound("total.lessThanOrEqual=" + DEFAULT_TOTAL);

        // Get all the saleList where total is less than or equal to SMALLER_TOTAL
        defaultSaleShouldNotBeFound("total.lessThanOrEqual=" + SMALLER_TOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where total is less than DEFAULT_TOTAL
        defaultSaleShouldNotBeFound("total.lessThan=" + DEFAULT_TOTAL);

        // Get all the saleList where total is less than UPDATED_TOTAL
        defaultSaleShouldBeFound("total.lessThan=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalIsGreaterThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where total is greater than DEFAULT_TOTAL
        defaultSaleShouldNotBeFound("total.greaterThan=" + DEFAULT_TOTAL);

        // Get all the saleList where total is greater than SMALLER_TOTAL
        defaultSaleShouldBeFound("total.greaterThan=" + SMALLER_TOTAL);
    }


    @Test
    @Transactional
    public void getAllSalesByTotalPaidIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where totalPaid equals to DEFAULT_TOTAL_PAID
        defaultSaleShouldBeFound("totalPaid.equals=" + DEFAULT_TOTAL_PAID);

        // Get all the saleList where totalPaid equals to UPDATED_TOTAL_PAID
        defaultSaleShouldNotBeFound("totalPaid.equals=" + UPDATED_TOTAL_PAID);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalPaidIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where totalPaid in DEFAULT_TOTAL_PAID or UPDATED_TOTAL_PAID
        defaultSaleShouldBeFound("totalPaid.in=" + DEFAULT_TOTAL_PAID + "," + UPDATED_TOTAL_PAID);

        // Get all the saleList where totalPaid equals to UPDATED_TOTAL_PAID
        defaultSaleShouldNotBeFound("totalPaid.in=" + UPDATED_TOTAL_PAID);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalPaidIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where totalPaid is not null
        defaultSaleShouldBeFound("totalPaid.specified=true");

        // Get all the saleList where totalPaid is null
        defaultSaleShouldNotBeFound("totalPaid.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByTotalPaidIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where totalPaid is greater than or equal to DEFAULT_TOTAL_PAID
        defaultSaleShouldBeFound("totalPaid.greaterThanOrEqual=" + DEFAULT_TOTAL_PAID);

        // Get all the saleList where totalPaid is greater than or equal to UPDATED_TOTAL_PAID
        defaultSaleShouldNotBeFound("totalPaid.greaterThanOrEqual=" + UPDATED_TOTAL_PAID);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalPaidIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where totalPaid is less than or equal to DEFAULT_TOTAL_PAID
        defaultSaleShouldBeFound("totalPaid.lessThanOrEqual=" + DEFAULT_TOTAL_PAID);

        // Get all the saleList where totalPaid is less than or equal to SMALLER_TOTAL_PAID
        defaultSaleShouldNotBeFound("totalPaid.lessThanOrEqual=" + SMALLER_TOTAL_PAID);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalPaidIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where totalPaid is less than DEFAULT_TOTAL_PAID
        defaultSaleShouldNotBeFound("totalPaid.lessThan=" + DEFAULT_TOTAL_PAID);

        // Get all the saleList where totalPaid is less than UPDATED_TOTAL_PAID
        defaultSaleShouldBeFound("totalPaid.lessThan=" + UPDATED_TOTAL_PAID);
    }

    @Test
    @Transactional
    public void getAllSalesByTotalPaidIsGreaterThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where totalPaid is greater than DEFAULT_TOTAL_PAID
        defaultSaleShouldNotBeFound("totalPaid.greaterThan=" + DEFAULT_TOTAL_PAID);

        // Get all the saleList where totalPaid is greater than SMALLER_TOTAL_PAID
        defaultSaleShouldBeFound("totalPaid.greaterThan=" + SMALLER_TOTAL_PAID);
    }


    @Test
    @Transactional
    public void getAllSalesByShoppingCartItemIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);
        ShoppingCartItem shoppingCartItem = ShoppingCartItemResourceIT.createEntity(em);
        em.persist(shoppingCartItem);
        em.flush();
        sale.addShoppingCartItem(shoppingCartItem);
        saleRepository.saveAndFlush(sale);
        Long shoppingCartItemId = shoppingCartItem.getId();

        // Get all the saleList where shoppingCartItem equals to shoppingCartItemId
        defaultSaleShouldBeFound("shoppingCartItemId.equals=" + shoppingCartItemId);

        // Get all the saleList where shoppingCartItem equals to shoppingCartItemId + 1
        defaultSaleShouldNotBeFound("shoppingCartItemId.equals=" + (shoppingCartItemId + 1));
    }


    @Test
    @Transactional
    public void getAllSalesByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        sale.setUser(user);
        saleRepository.saveAndFlush(sale);
        Long userId = user.getId();

        // Get all the saleList where user equals to userId
        defaultSaleShouldBeFound("userId.equals=" + userId);

        // Get all the saleList where user equals to userId + 1
        defaultSaleShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSaleShouldBeFound(String filter) throws Exception {
        restSaleMockMvc.perform(get("/api/sales?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sale.getId().intValue())))
            .andExpect(jsonPath("$.[*].subtotal").value(hasItem(DEFAULT_SUBTOTAL.intValue())))
            .andExpect(jsonPath("$.[*].productsQuantity").value(hasItem(DEFAULT_PRODUCTS_QUANTITY)))
            .andExpect(jsonPath("$.[*].discount").value(hasItem(DEFAULT_DISCOUNT.intValue())))
            .andExpect(jsonPath("$.[*].iva").value(hasItem(DEFAULT_IVA.intValue())))
            .andExpect(jsonPath("$.[*].invoiceBusinessName").value(hasItem(DEFAULT_INVOICE_BUSINESS_NAME)))
            .andExpect(jsonPath("$.[*].nitDocument").value(hasItem(DEFAULT_NIT_DOCUMENT)))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.intValue())))
            .andExpect(jsonPath("$.[*].totalPaid").value(hasItem(DEFAULT_TOTAL_PAID.intValue())));

        // Check, that the count call also returns 1
        restSaleMockMvc.perform(get("/api/sales/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSaleShouldNotBeFound(String filter) throws Exception {
        restSaleMockMvc.perform(get("/api/sales?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSaleMockMvc.perform(get("/api/sales/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSale() throws Exception {
        // Get the sale
        restSaleMockMvc.perform(get("/api/sales/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSale() throws Exception {
        // Initialize the database
        saleService.save(sale);

        int databaseSizeBeforeUpdate = saleRepository.findAll().size();

        // Update the sale
        Sale updatedSale = saleRepository.findById(sale.getId()).get();
        // Disconnect from session so that the updates on updatedSale are not directly saved in db
        em.detach(updatedSale);
        updatedSale
            .subtotal(UPDATED_SUBTOTAL)
            .productsQuantity(UPDATED_PRODUCTS_QUANTITY)
            .discount(UPDATED_DISCOUNT)
            .iva(UPDATED_IVA)
            .invoiceBusinessName(UPDATED_INVOICE_BUSINESS_NAME)
            .nitDocument(UPDATED_NIT_DOCUMENT)
            .total(UPDATED_TOTAL)
            .totalPaid(UPDATED_TOTAL_PAID);

        restSaleMockMvc.perform(put("/api/sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSale)))
            .andExpect(status().isOk());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeUpdate);
        Sale testSale = saleList.get(saleList.size() - 1);
        assertThat(testSale.getSubtotal()).isEqualTo(UPDATED_SUBTOTAL);
        assertThat(testSale.getProductsQuantity()).isEqualTo(UPDATED_PRODUCTS_QUANTITY);
        assertThat(testSale.getDiscount()).isEqualTo(UPDATED_DISCOUNT);
        assertThat(testSale.getIva()).isEqualTo(UPDATED_IVA);
        assertThat(testSale.getInvoiceBusinessName()).isEqualTo(UPDATED_INVOICE_BUSINESS_NAME);
        assertThat(testSale.getNitDocument()).isEqualTo(UPDATED_NIT_DOCUMENT);
        assertThat(testSale.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testSale.getTotalPaid()).isEqualTo(UPDATED_TOTAL_PAID);
    }

    @Test
    @Transactional
    public void updateNonExistingSale() throws Exception {
        int databaseSizeBeforeUpdate = saleRepository.findAll().size();

        // Create the Sale

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSaleMockMvc.perform(put("/api/sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sale)))
            .andExpect(status().isBadRequest());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSale() throws Exception {
        // Initialize the database
        saleService.save(sale);

        int databaseSizeBeforeDelete = saleRepository.findAll().size();

        // Delete the sale
        restSaleMockMvc.perform(delete("/api/sales/{id}", sale.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sale.class);
        Sale sale1 = new Sale();
        sale1.setId(1L);
        Sale sale2 = new Sale();
        sale2.setId(sale1.getId());
        assertThat(sale1).isEqualTo(sale2);
        sale2.setId(2L);
        assertThat(sale1).isNotEqualTo(sale2);
        sale1.setId(null);
        assertThat(sale1).isNotEqualTo(sale2);
    }
}
