import { IShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';
import { IUser } from 'app/core/user/user.model';

export interface ISale {
  id?: number;
  subtotal?: number;
  productsQuantity?: number;
  discount?: number;
  iva?: number;
  invoiceBusinessName?: string;
  nitDocument?: string;
  total?: number;
  totalPaid?: number;
  shoppingCartItems?: IShoppingCartItem[];
  user?: IUser;
}

export class Sale implements ISale {
  constructor(
    public id?: number,
    public subtotal?: number,
    public productsQuantity?: number,
    public discount?: number,
    public iva?: number,
    public invoiceBusinessName?: string,
    public nitDocument?: string,
    public total?: number,
    public totalPaid?: number,
    public shoppingCartItems?: IShoppingCartItem[],
    public user?: IUser
  ) {}
}
