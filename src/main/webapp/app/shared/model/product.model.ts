import { IShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';

export const enum ProductStatus {
  AVAILABLE = 'AVAILABLE',
  NOT_AVAILABLE = 'NOT_AVAILABLE',
  NOT_PUBLISHED = 'NOT_PUBLISHED'
}

export interface IProduct {
  id?: number;
  name?: string;
  barcode?: string;
  detail?: any;
  price?: number;
  imageContentType?: string;
  image?: any;
  status?: ProductStatus;
  shoppingCartItems?: IShoppingCartItem[];
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public barcode?: string,
    public detail?: any,
    public price?: number,
    public imageContentType?: string,
    public image?: any,
    public status?: ProductStatus,
    public shoppingCartItems?: IShoppingCartItem[]
  ) {}
}
