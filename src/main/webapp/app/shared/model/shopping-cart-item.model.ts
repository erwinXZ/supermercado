import { IProduct } from 'app/shared/model/product.model';
import { ISale } from 'app/shared/model/sale.model';

export interface IShoppingCartItem {
  id?: number;
  total?: number;
  productQuantity?: number;
  product?: IProduct;
  sale?: ISale;
}

export class ShoppingCartItem implements IShoppingCartItem {
  constructor(public id?: number, public total?: number, public productQuantity?: number, public product?: IProduct, public sale?: ISale) {}
}
