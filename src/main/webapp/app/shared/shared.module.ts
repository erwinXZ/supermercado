import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SupermercadoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [SupermercadoSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [SupermercadoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupermercadoSharedModule {
  static forRoot() {
    return {
      ngModule: SupermercadoSharedModule
    };
  }
}
