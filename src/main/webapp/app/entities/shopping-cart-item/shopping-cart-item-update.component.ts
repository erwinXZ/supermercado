import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IShoppingCartItem, ShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';
import { ShoppingCartItemService } from './shopping-cart-item.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product';
import { ISale } from 'app/shared/model/sale.model';
import { SaleService } from 'app/entities/sale';

@Component({
  selector: 'jhi-shopping-cart-item-update',
  templateUrl: './shopping-cart-item-update.component.html'
})
export class ShoppingCartItemUpdateComponent implements OnInit {
  isSaving: boolean;

  products: IProduct[];

  sales: ISale[];

  editForm = this.fb.group({
    id: [],
    total: [],
    productQuantity: [],
    product: [],
    sale: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shoppingCartItemService: ShoppingCartItemService,
    protected productService: ProductService,
    protected saleService: SaleService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shoppingCartItem }) => {
      this.updateForm(shoppingCartItem);
    });
    this.productService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProduct[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProduct[]>) => response.body)
      )
      .subscribe((res: IProduct[]) => (this.products = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.saleService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ISale[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISale[]>) => response.body)
      )
      .subscribe((res: ISale[]) => (this.sales = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shoppingCartItem: IShoppingCartItem) {
    this.editForm.patchValue({
      id: shoppingCartItem.id,
      total: shoppingCartItem.total,
      productQuantity: shoppingCartItem.productQuantity,
      product: shoppingCartItem.product,
      sale: shoppingCartItem.sale
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shoppingCartItem = this.createFromForm();
    if (shoppingCartItem.id !== undefined) {
      this.subscribeToSaveResponse(this.shoppingCartItemService.update(shoppingCartItem));
    } else {
      this.subscribeToSaveResponse(this.shoppingCartItemService.create(shoppingCartItem));
    }
  }

  private createFromForm(): IShoppingCartItem {
    return {
      ...new ShoppingCartItem(),
      id: this.editForm.get(['id']).value,
      total: this.editForm.get(['total']).value,
      productQuantity: this.editForm.get(['productQuantity']).value,
      product: this.editForm.get(['product']).value,
      sale: this.editForm.get(['sale']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShoppingCartItem>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProductById(index: number, item: IProduct) {
    return item.id;
  }

  trackSaleById(index: number, item: ISale) {
    return item.id;
  }
}
