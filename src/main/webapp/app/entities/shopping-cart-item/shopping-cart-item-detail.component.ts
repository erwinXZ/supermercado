import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';

@Component({
  selector: 'jhi-shopping-cart-item-detail',
  templateUrl: './shopping-cart-item-detail.component.html'
})
export class ShoppingCartItemDetailComponent implements OnInit {
  shoppingCartItem: IShoppingCartItem;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shoppingCartItem }) => {
      this.shoppingCartItem = shoppingCartItem;
    });
  }

  previousState() {
    window.history.back();
  }
}
