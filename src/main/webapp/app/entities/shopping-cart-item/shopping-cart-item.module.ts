import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SupermercadoSharedModule } from 'app/shared';
import {
  ShoppingCartItemComponent,
  ShoppingCartItemDetailComponent,
  ShoppingCartItemUpdateComponent,
  ShoppingCartItemDeletePopupComponent,
  ShoppingCartItemDeleteDialogComponent,
  shoppingCartItemRoute,
  shoppingCartItemPopupRoute
} from './';

const ENTITY_STATES = [...shoppingCartItemRoute, ...shoppingCartItemPopupRoute];

@NgModule({
  imports: [SupermercadoSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ShoppingCartItemComponent,
    ShoppingCartItemDetailComponent,
    ShoppingCartItemUpdateComponent,
    ShoppingCartItemDeleteDialogComponent,
    ShoppingCartItemDeletePopupComponent
  ],
  entryComponents: [
    ShoppingCartItemComponent,
    ShoppingCartItemUpdateComponent,
    ShoppingCartItemDeleteDialogComponent,
    ShoppingCartItemDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupermercadoShoppingCartItemModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
