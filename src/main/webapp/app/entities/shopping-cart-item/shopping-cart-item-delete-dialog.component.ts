import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';
import { ShoppingCartItemService } from './shopping-cart-item.service';

@Component({
  selector: 'jhi-shopping-cart-item-delete-dialog',
  templateUrl: './shopping-cart-item-delete-dialog.component.html'
})
export class ShoppingCartItemDeleteDialogComponent {
  shoppingCartItem: IShoppingCartItem;

  constructor(
    protected shoppingCartItemService: ShoppingCartItemService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.shoppingCartItemService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'shoppingCartItemListModification',
        content: 'Deleted an shoppingCartItem'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-shopping-cart-item-delete-popup',
  template: ''
})
export class ShoppingCartItemDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shoppingCartItem }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ShoppingCartItemDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.shoppingCartItem = shoppingCartItem;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/shopping-cart-item', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/shopping-cart-item', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
