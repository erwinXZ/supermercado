import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';
import { ShoppingCartItemService } from './shopping-cart-item.service';
import { ShoppingCartItemComponent } from './shopping-cart-item.component';
import { ShoppingCartItemDetailComponent } from './shopping-cart-item-detail.component';
import { ShoppingCartItemUpdateComponent } from './shopping-cart-item-update.component';
import { ShoppingCartItemDeletePopupComponent } from './shopping-cart-item-delete-dialog.component';
import { IShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';

@Injectable({ providedIn: 'root' })
export class ShoppingCartItemResolve implements Resolve<IShoppingCartItem> {
  constructor(private service: ShoppingCartItemService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShoppingCartItem> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShoppingCartItem>) => response.ok),
        map((shoppingCartItem: HttpResponse<ShoppingCartItem>) => shoppingCartItem.body)
      );
    }
    return of(new ShoppingCartItem());
  }
}

export const shoppingCartItemRoute: Routes = [
  {
    path: '',
    component: ShoppingCartItemComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'supermercadoApp.shoppingCartItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShoppingCartItemDetailComponent,
    resolve: {
      shoppingCartItem: ShoppingCartItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'supermercadoApp.shoppingCartItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShoppingCartItemUpdateComponent,
    resolve: {
      shoppingCartItem: ShoppingCartItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'supermercadoApp.shoppingCartItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShoppingCartItemUpdateComponent,
    resolve: {
      shoppingCartItem: ShoppingCartItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'supermercadoApp.shoppingCartItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shoppingCartItemPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShoppingCartItemDeletePopupComponent,
    resolve: {
      shoppingCartItem: ShoppingCartItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'supermercadoApp.shoppingCartItem.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
