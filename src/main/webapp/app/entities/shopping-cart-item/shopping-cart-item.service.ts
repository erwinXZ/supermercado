import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShoppingCartItem } from 'app/shared/model/shopping-cart-item.model';

type EntityResponseType = HttpResponse<IShoppingCartItem>;
type EntityArrayResponseType = HttpResponse<IShoppingCartItem[]>;

@Injectable({ providedIn: 'root' })
export class ShoppingCartItemService {
  public resourceUrl = SERVER_API_URL + 'api/shopping-cart-items';

  constructor(protected http: HttpClient) {}

  create(shoppingCartItem: IShoppingCartItem): Observable<EntityResponseType> {
    return this.http.post<IShoppingCartItem>(this.resourceUrl, shoppingCartItem, { observe: 'response' });
  }

  update(shoppingCartItem: IShoppingCartItem): Observable<EntityResponseType> {
    return this.http.put<IShoppingCartItem>(this.resourceUrl, shoppingCartItem, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IShoppingCartItem>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShoppingCartItem[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
