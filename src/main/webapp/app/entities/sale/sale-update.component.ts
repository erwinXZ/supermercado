import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISale, Sale } from 'app/shared/model/sale.model';
import { SaleService } from './sale.service';
import { IUser, UserService } from 'app/core';

@Component({
  selector: 'jhi-sale-update',
  templateUrl: './sale-update.component.html'
})
export class SaleUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    subtotal: [],
    productsQuantity: [],
    discount: [],
    iva: [],
    invoiceBusinessName: [],
    nitDocument: [],
    total: [],
    totalPaid: [],
    user: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected saleService: SaleService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ sale }) => {
      this.updateForm(sale);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(sale: ISale) {
    this.editForm.patchValue({
      id: sale.id,
      subtotal: sale.subtotal,
      productsQuantity: sale.productsQuantity,
      discount: sale.discount,
      iva: sale.iva,
      invoiceBusinessName: sale.invoiceBusinessName,
      nitDocument: sale.nitDocument,
      total: sale.total,
      totalPaid: sale.totalPaid,
      user: sale.user
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const sale = this.createFromForm();
    if (sale.id !== undefined) {
      this.subscribeToSaveResponse(this.saleService.update(sale));
    } else {
      this.subscribeToSaveResponse(this.saleService.create(sale));
    }
  }

  private createFromForm(): ISale {
    return {
      ...new Sale(),
      id: this.editForm.get(['id']).value,
      subtotal: this.editForm.get(['subtotal']).value,
      productsQuantity: this.editForm.get(['productsQuantity']).value,
      discount: this.editForm.get(['discount']).value,
      iva: this.editForm.get(['iva']).value,
      invoiceBusinessName: this.editForm.get(['invoiceBusinessName']).value,
      nitDocument: this.editForm.get(['nitDocument']).value,
      total: this.editForm.get(['total']).value,
      totalPaid: this.editForm.get(['totalPaid']).value,
      user: this.editForm.get(['user']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISale>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
