import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SupermercadoSharedModule } from 'app/shared';
import {
  SaleComponent,
  SaleDetailComponent,
  SaleUpdateComponent,
  SaleDeletePopupComponent,
  SaleDeleteDialogComponent,
  saleRoute,
  salePopupRoute
} from './';

const ENTITY_STATES = [...saleRoute, ...salePopupRoute];

@NgModule({
  imports: [SupermercadoSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [SaleComponent, SaleDetailComponent, SaleUpdateComponent, SaleDeleteDialogComponent, SaleDeletePopupComponent],
  entryComponents: [SaleComponent, SaleUpdateComponent, SaleDeleteDialogComponent, SaleDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupermercadoSaleModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
