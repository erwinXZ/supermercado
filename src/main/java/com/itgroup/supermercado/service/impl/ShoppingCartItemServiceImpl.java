package com.itgroup.supermercado.service.impl;

import com.itgroup.supermercado.service.ShoppingCartItemService;
import com.itgroup.supermercado.domain.ShoppingCartItem;
import com.itgroup.supermercado.repository.ShoppingCartItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ShoppingCartItem}.
 */
@Service
@Transactional
public class ShoppingCartItemServiceImpl implements ShoppingCartItemService {

    private final Logger log = LoggerFactory.getLogger(ShoppingCartItemServiceImpl.class);

    private final ShoppingCartItemRepository shoppingCartItemRepository;

    public ShoppingCartItemServiceImpl(ShoppingCartItemRepository shoppingCartItemRepository) {
        this.shoppingCartItemRepository = shoppingCartItemRepository;
    }

    /**
     * Save a shoppingCartItem.
     *
     * @param shoppingCartItem the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShoppingCartItem save(ShoppingCartItem shoppingCartItem) {
        log.debug("Request to save ShoppingCartItem : {}", shoppingCartItem);
        return shoppingCartItemRepository.save(shoppingCartItem);
    }

    /**
     * Get all the shoppingCartItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShoppingCartItem> findAll(Pageable pageable) {
        log.debug("Request to get all ShoppingCartItems");
        return shoppingCartItemRepository.findAll(pageable);
    }


    /**
     * Get one shoppingCartItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShoppingCartItem> findOne(Long id) {
        log.debug("Request to get ShoppingCartItem : {}", id);
        return shoppingCartItemRepository.findById(id);
    }

    /**
     * Delete the shoppingCartItem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShoppingCartItem : {}", id);
        shoppingCartItemRepository.deleteById(id);
    }
}
