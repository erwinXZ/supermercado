package com.itgroup.supermercado.service;

import com.itgroup.supermercado.domain.ShoppingCartItem;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ShoppingCartItem}.
 */
public interface ShoppingCartItemService {

    /**
     * Save a shoppingCartItem.
     *
     * @param shoppingCartItem the entity to save.
     * @return the persisted entity.
     */
    ShoppingCartItem save(ShoppingCartItem shoppingCartItem);

    /**
     * Get all the shoppingCartItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShoppingCartItem> findAll(Pageable pageable);


    /**
     * Get the "id" shoppingCartItem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShoppingCartItem> findOne(Long id);

    /**
     * Delete the "id" shoppingCartItem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
