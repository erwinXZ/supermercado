package com.itgroup.supermercado.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.itgroup.supermercado.domain.Sale;
import com.itgroup.supermercado.domain.*; // for static metamodels
import com.itgroup.supermercado.repository.SaleRepository;
import com.itgroup.supermercado.service.dto.SaleCriteria;

/**
 * Service for executing complex queries for {@link Sale} entities in the database.
 * The main input is a {@link SaleCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Sale} or a {@link Page} of {@link Sale} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SaleQueryService extends QueryService<Sale> {

    private final Logger log = LoggerFactory.getLogger(SaleQueryService.class);

    private final SaleRepository saleRepository;

    public SaleQueryService(SaleRepository saleRepository) {
        this.saleRepository = saleRepository;
    }

    /**
     * Return a {@link List} of {@link Sale} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Sale> findByCriteria(SaleCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Sale> specification = createSpecification(criteria);
        return saleRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Sale} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Sale> findByCriteria(SaleCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Sale> specification = createSpecification(criteria);
        return saleRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SaleCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Sale> specification = createSpecification(criteria);
        return saleRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<Sale> createSpecification(SaleCriteria criteria) {
        Specification<Sale> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Sale_.id));
            }
            if (criteria.getSubtotal() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSubtotal(), Sale_.subtotal));
            }
            if (criteria.getProductsQuantity() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProductsQuantity(), Sale_.productsQuantity));
            }
            if (criteria.getDiscount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDiscount(), Sale_.discount));
            }
            if (criteria.getIva() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIva(), Sale_.iva));
            }
            if (criteria.getInvoiceBusinessName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInvoiceBusinessName(), Sale_.invoiceBusinessName));
            }
            if (criteria.getNitDocument() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNitDocument(), Sale_.nitDocument));
            }
            if (criteria.getTotal() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotal(), Sale_.total));
            }
            if (criteria.getTotalPaid() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalPaid(), Sale_.totalPaid));
            }
            if (criteria.getShoppingCartItemId() != null) {
                specification = specification.and(buildSpecification(criteria.getShoppingCartItemId(),
                    root -> root.join(Sale_.shoppingCartItems, JoinType.LEFT).get(ShoppingCartItem_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Sale_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
