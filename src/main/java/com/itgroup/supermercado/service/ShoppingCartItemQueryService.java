package com.itgroup.supermercado.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.itgroup.supermercado.domain.ShoppingCartItem;
import com.itgroup.supermercado.domain.*; // for static metamodels
import com.itgroup.supermercado.repository.ShoppingCartItemRepository;
import com.itgroup.supermercado.service.dto.ShoppingCartItemCriteria;

/**
 * Service for executing complex queries for {@link ShoppingCartItem} entities in the database.
 * The main input is a {@link ShoppingCartItemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShoppingCartItem} or a {@link Page} of {@link ShoppingCartItem} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShoppingCartItemQueryService extends QueryService<ShoppingCartItem> {

    private final Logger log = LoggerFactory.getLogger(ShoppingCartItemQueryService.class);

    private final ShoppingCartItemRepository shoppingCartItemRepository;

    public ShoppingCartItemQueryService(ShoppingCartItemRepository shoppingCartItemRepository) {
        this.shoppingCartItemRepository = shoppingCartItemRepository;
    }

    /**
     * Return a {@link List} of {@link ShoppingCartItem} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShoppingCartItem> findByCriteria(ShoppingCartItemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShoppingCartItem> specification = createSpecification(criteria);
        return shoppingCartItemRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ShoppingCartItem} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingCartItem> findByCriteria(ShoppingCartItemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShoppingCartItem> specification = createSpecification(criteria);
        return shoppingCartItemRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShoppingCartItemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShoppingCartItem> specification = createSpecification(criteria);
        return shoppingCartItemRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<ShoppingCartItem> createSpecification(ShoppingCartItemCriteria criteria) {
        Specification<ShoppingCartItem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShoppingCartItem_.id));
            }
            if (criteria.getTotal() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotal(), ShoppingCartItem_.total));
            }
            if (criteria.getProductQuantity() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProductQuantity(), ShoppingCartItem_.productQuantity));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductId(),
                    root -> root.join(ShoppingCartItem_.product, JoinType.LEFT).get(Product_.id)));
            }
            if (criteria.getSaleId() != null) {
                specification = specification.and(buildSpecification(criteria.getSaleId(),
                    root -> root.join(ShoppingCartItem_.sale, JoinType.LEFT).get(Sale_.id)));
            }
        }
        return specification;
    }
}
