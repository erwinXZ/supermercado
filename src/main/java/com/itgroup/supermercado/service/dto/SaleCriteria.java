package com.itgroup.supermercado.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link com.itgroup.supermercado.domain.Sale} entity. This class is used
 * in {@link com.itgroup.supermercado.web.rest.SaleResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /sales?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SaleCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BigDecimalFilter subtotal;

    private IntegerFilter productsQuantity;

    private BigDecimalFilter discount;

    private BigDecimalFilter iva;

    private StringFilter invoiceBusinessName;

    private StringFilter nitDocument;

    private BigDecimalFilter total;

    private BigDecimalFilter totalPaid;

    private LongFilter shoppingCartItemId;

    private LongFilter userId;

    public SaleCriteria(){
    }

    public SaleCriteria(SaleCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.subtotal = other.subtotal == null ? null : other.subtotal.copy();
        this.productsQuantity = other.productsQuantity == null ? null : other.productsQuantity.copy();
        this.discount = other.discount == null ? null : other.discount.copy();
        this.iva = other.iva == null ? null : other.iva.copy();
        this.invoiceBusinessName = other.invoiceBusinessName == null ? null : other.invoiceBusinessName.copy();
        this.nitDocument = other.nitDocument == null ? null : other.nitDocument.copy();
        this.total = other.total == null ? null : other.total.copy();
        this.totalPaid = other.totalPaid == null ? null : other.totalPaid.copy();
        this.shoppingCartItemId = other.shoppingCartItemId == null ? null : other.shoppingCartItemId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public SaleCriteria copy() {
        return new SaleCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BigDecimalFilter getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimalFilter subtotal) {
        this.subtotal = subtotal;
    }

    public IntegerFilter getProductsQuantity() {
        return productsQuantity;
    }

    public void setProductsQuantity(IntegerFilter productsQuantity) {
        this.productsQuantity = productsQuantity;
    }

    public BigDecimalFilter getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimalFilter discount) {
        this.discount = discount;
    }

    public BigDecimalFilter getIva() {
        return iva;
    }

    public void setIva(BigDecimalFilter iva) {
        this.iva = iva;
    }

    public StringFilter getInvoiceBusinessName() {
        return invoiceBusinessName;
    }

    public void setInvoiceBusinessName(StringFilter invoiceBusinessName) {
        this.invoiceBusinessName = invoiceBusinessName;
    }

    public StringFilter getNitDocument() {
        return nitDocument;
    }

    public void setNitDocument(StringFilter nitDocument) {
        this.nitDocument = nitDocument;
    }

    public BigDecimalFilter getTotal() {
        return total;
    }

    public void setTotal(BigDecimalFilter total) {
        this.total = total;
    }

    public BigDecimalFilter getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(BigDecimalFilter totalPaid) {
        this.totalPaid = totalPaid;
    }

    public LongFilter getShoppingCartItemId() {
        return shoppingCartItemId;
    }

    public void setShoppingCartItemId(LongFilter shoppingCartItemId) {
        this.shoppingCartItemId = shoppingCartItemId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SaleCriteria that = (SaleCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(subtotal, that.subtotal) &&
            Objects.equals(productsQuantity, that.productsQuantity) &&
            Objects.equals(discount, that.discount) &&
            Objects.equals(iva, that.iva) &&
            Objects.equals(invoiceBusinessName, that.invoiceBusinessName) &&
            Objects.equals(nitDocument, that.nitDocument) &&
            Objects.equals(total, that.total) &&
            Objects.equals(totalPaid, that.totalPaid) &&
            Objects.equals(shoppingCartItemId, that.shoppingCartItemId) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        subtotal,
        productsQuantity,
        discount,
        iva,
        invoiceBusinessName,
        nitDocument,
        total,
        totalPaid,
        shoppingCartItemId,
        userId
        );
    }

    @Override
    public String toString() {
        return "SaleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (subtotal != null ? "subtotal=" + subtotal + ", " : "") +
                (productsQuantity != null ? "productsQuantity=" + productsQuantity + ", " : "") +
                (discount != null ? "discount=" + discount + ", " : "") +
                (iva != null ? "iva=" + iva + ", " : "") +
                (invoiceBusinessName != null ? "invoiceBusinessName=" + invoiceBusinessName + ", " : "") +
                (nitDocument != null ? "nitDocument=" + nitDocument + ", " : "") +
                (total != null ? "total=" + total + ", " : "") +
                (totalPaid != null ? "totalPaid=" + totalPaid + ", " : "") +
                (shoppingCartItemId != null ? "shoppingCartItemId=" + shoppingCartItemId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
