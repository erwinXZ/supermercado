package com.itgroup.supermercado.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link com.itgroup.supermercado.domain.ShoppingCartItem} entity. This class is used
 * in {@link com.itgroup.supermercado.web.rest.ShoppingCartItemResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shopping-cart-items?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShoppingCartItemCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BigDecimalFilter total;

    private IntegerFilter productQuantity;

    private LongFilter productId;

    private LongFilter saleId;

    public ShoppingCartItemCriteria(){
    }

    public ShoppingCartItemCriteria(ShoppingCartItemCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.total = other.total == null ? null : other.total.copy();
        this.productQuantity = other.productQuantity == null ? null : other.productQuantity.copy();
        this.productId = other.productId == null ? null : other.productId.copy();
        this.saleId = other.saleId == null ? null : other.saleId.copy();
    }

    @Override
    public ShoppingCartItemCriteria copy() {
        return new ShoppingCartItemCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BigDecimalFilter getTotal() {
        return total;
    }

    public void setTotal(BigDecimalFilter total) {
        this.total = total;
    }

    public IntegerFilter getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(IntegerFilter productQuantity) {
        this.productQuantity = productQuantity;
    }

    public LongFilter getProductId() {
        return productId;
    }

    public void setProductId(LongFilter productId) {
        this.productId = productId;
    }

    public LongFilter getSaleId() {
        return saleId;
    }

    public void setSaleId(LongFilter saleId) {
        this.saleId = saleId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShoppingCartItemCriteria that = (ShoppingCartItemCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(total, that.total) &&
            Objects.equals(productQuantity, that.productQuantity) &&
            Objects.equals(productId, that.productId) &&
            Objects.equals(saleId, that.saleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        total,
        productQuantity,
        productId,
        saleId
        );
    }

    @Override
    public String toString() {
        return "ShoppingCartItemCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (total != null ? "total=" + total + ", " : "") +
                (productQuantity != null ? "productQuantity=" + productQuantity + ", " : "") +
                (productId != null ? "productId=" + productId + ", " : "") +
                (saleId != null ? "saleId=" + saleId + ", " : "") +
            "}";
    }

}
