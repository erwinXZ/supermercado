package com.itgroup.supermercado.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.itgroup.supermercado.domain.enumeration.ProductStatus;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link com.itgroup.supermercado.domain.Product} entity. This class is used
 * in {@link com.itgroup.supermercado.web.rest.ProductResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /products?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProductCriteria implements Serializable, Criteria {
    /**
     * Class for filtering ProductStatus
     */
    public static class ProductStatusFilter extends Filter<ProductStatus> {

        public ProductStatusFilter() {
        }

        public ProductStatusFilter(ProductStatusFilter filter) {
            super(filter);
        }

        @Override
        public ProductStatusFilter copy() {
            return new ProductStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter barcode;

    private BigDecimalFilter price;

    private ProductStatusFilter status;

    private LongFilter shoppingCartItemId;

    public ProductCriteria(){
    }

    public ProductCriteria(ProductCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.barcode = other.barcode == null ? null : other.barcode.copy();
        this.price = other.price == null ? null : other.price.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.shoppingCartItemId = other.shoppingCartItemId == null ? null : other.shoppingCartItemId.copy();
    }

    @Override
    public ProductCriteria copy() {
        return new ProductCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getBarcode() {
        return barcode;
    }

    public void setBarcode(StringFilter barcode) {
        this.barcode = barcode;
    }

    public BigDecimalFilter getPrice() {
        return price;
    }

    public void setPrice(BigDecimalFilter price) {
        this.price = price;
    }

    public ProductStatusFilter getStatus() {
        return status;
    }

    public void setStatus(ProductStatusFilter status) {
        this.status = status;
    }

    public LongFilter getShoppingCartItemId() {
        return shoppingCartItemId;
    }

    public void setShoppingCartItemId(LongFilter shoppingCartItemId) {
        this.shoppingCartItemId = shoppingCartItemId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProductCriteria that = (ProductCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(barcode, that.barcode) &&
            Objects.equals(price, that.price) &&
            Objects.equals(status, that.status) &&
            Objects.equals(shoppingCartItemId, that.shoppingCartItemId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        barcode,
        price,
        status,
        shoppingCartItemId
        );
    }

    @Override
    public String toString() {
        return "ProductCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (barcode != null ? "barcode=" + barcode + ", " : "") +
                (price != null ? "price=" + price + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (shoppingCartItemId != null ? "shoppingCartItemId=" + shoppingCartItemId + ", " : "") +
            "}";
    }

}
