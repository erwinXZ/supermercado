/**
 * View Models used by Spring MVC REST controllers.
 */
package com.itgroup.supermercado.web.rest.vm;
