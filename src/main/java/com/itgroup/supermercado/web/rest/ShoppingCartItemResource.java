package com.itgroup.supermercado.web.rest;

import com.itgroup.supermercado.domain.ShoppingCartItem;
import com.itgroup.supermercado.service.ShoppingCartItemService;
import com.itgroup.supermercado.web.rest.errors.BadRequestAlertException;
import com.itgroup.supermercado.service.dto.ShoppingCartItemCriteria;
import com.itgroup.supermercado.service.ShoppingCartItemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.itgroup.supermercado.domain.ShoppingCartItem}.
 */
@RestController
@RequestMapping("/api")
public class ShoppingCartItemResource {

    private final Logger log = LoggerFactory.getLogger(ShoppingCartItemResource.class);

    private static final String ENTITY_NAME = "shoppingCartItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShoppingCartItemService shoppingCartItemService;

    private final ShoppingCartItemQueryService shoppingCartItemQueryService;

    public ShoppingCartItemResource(ShoppingCartItemService shoppingCartItemService, ShoppingCartItemQueryService shoppingCartItemQueryService) {
        this.shoppingCartItemService = shoppingCartItemService;
        this.shoppingCartItemQueryService = shoppingCartItemQueryService;
    }

    /**
     * {@code POST  /shopping-cart-items} : Create a new shoppingCartItem.
     *
     * @param shoppingCartItem the shoppingCartItem to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shoppingCartItem, or with status {@code 400 (Bad Request)} if the shoppingCartItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shopping-cart-items")
    public ResponseEntity<ShoppingCartItem> createShoppingCartItem(@RequestBody ShoppingCartItem shoppingCartItem) throws URISyntaxException {
        log.debug("REST request to save ShoppingCartItem : {}", shoppingCartItem);
        if (shoppingCartItem.getId() != null) {
            throw new BadRequestAlertException("A new shoppingCartItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShoppingCartItem result = shoppingCartItemService.save(shoppingCartItem);
        return ResponseEntity.created(new URI("/api/shopping-cart-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shopping-cart-items} : Updates an existing shoppingCartItem.
     *
     * @param shoppingCartItem the shoppingCartItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shoppingCartItem,
     * or with status {@code 400 (Bad Request)} if the shoppingCartItem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shoppingCartItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shopping-cart-items")
    public ResponseEntity<ShoppingCartItem> updateShoppingCartItem(@RequestBody ShoppingCartItem shoppingCartItem) throws URISyntaxException {
        log.debug("REST request to update ShoppingCartItem : {}", shoppingCartItem);
        if (shoppingCartItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShoppingCartItem result = shoppingCartItemService.save(shoppingCartItem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shoppingCartItem.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shopping-cart-items} : get all the shoppingCartItems.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shoppingCartItems in body.
     */
    @GetMapping("/shopping-cart-items")
    public ResponseEntity<List<ShoppingCartItem>> getAllShoppingCartItems(ShoppingCartItemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ShoppingCartItems by criteria: {}", criteria);
        Page<ShoppingCartItem> page = shoppingCartItemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /shopping-cart-items/count} : count all the shoppingCartItems.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/shopping-cart-items/count")
    public ResponseEntity<Long> countShoppingCartItems(ShoppingCartItemCriteria criteria) {
        log.debug("REST request to count ShoppingCartItems by criteria: {}", criteria);
        return ResponseEntity.ok().body(shoppingCartItemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shopping-cart-items/:id} : get the "id" shoppingCartItem.
     *
     * @param id the id of the shoppingCartItem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shoppingCartItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shopping-cart-items/{id}")
    public ResponseEntity<ShoppingCartItem> getShoppingCartItem(@PathVariable Long id) {
        log.debug("REST request to get ShoppingCartItem : {}", id);
        Optional<ShoppingCartItem> shoppingCartItem = shoppingCartItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shoppingCartItem);
    }

    /**
     * {@code DELETE  /shopping-cart-items/:id} : delete the "id" shoppingCartItem.
     *
     * @param id the id of the shoppingCartItem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shopping-cart-items/{id}")
    public ResponseEntity<Void> deleteShoppingCartItem(@PathVariable Long id) {
        log.debug("REST request to delete ShoppingCartItem : {}", id);
        shoppingCartItemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
