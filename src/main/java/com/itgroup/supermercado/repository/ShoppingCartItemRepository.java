package com.itgroup.supermercado.repository;

import com.itgroup.supermercado.domain.ShoppingCartItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ShoppingCartItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShoppingCartItemRepository extends JpaRepository<ShoppingCartItem, Long>, JpaSpecificationExecutor<ShoppingCartItem> {

}
