package com.itgroup.supermercado.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * A Sale.
 */
@Entity
@Table(name = "sale")
public class Sale implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "subtotal", precision = 21, scale = 2)
    private BigDecimal subtotal;

    @Column(name = "products_quantity")
    private Integer productsQuantity;

    @Column(name = "discount", precision = 21, scale = 2)
    private BigDecimal discount;

    @Column(name = "iva", precision = 21, scale = 2)
    private BigDecimal iva;

    @Column(name = "invoice_business_name")
    private String invoiceBusinessName;

    @Column(name = "nit_document")
    private String nitDocument;

    @Column(name = "total", precision = 21, scale = 2)
    private BigDecimal total;

    @Column(name = "total_paid", precision = 21, scale = 2)
    private BigDecimal totalPaid;

    @OneToMany(mappedBy = "sale")
    private Set<ShoppingCartItem> shoppingCartItems = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("sales")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public Sale subtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
        return this;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public Integer getProductsQuantity() {
        return productsQuantity;
    }

    public Sale productsQuantity(Integer productsQuantity) {
        this.productsQuantity = productsQuantity;
        return this;
    }

    public void setProductsQuantity(Integer productsQuantity) {
        this.productsQuantity = productsQuantity;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public Sale discount(BigDecimal discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public Sale iva(BigDecimal iva) {
        this.iva = iva;
        return this;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    public String getInvoiceBusinessName() {
        return invoiceBusinessName;
    }

    public Sale invoiceBusinessName(String invoiceBusinessName) {
        this.invoiceBusinessName = invoiceBusinessName;
        return this;
    }

    public void setInvoiceBusinessName(String invoiceBusinessName) {
        this.invoiceBusinessName = invoiceBusinessName;
    }

    public String getNitDocument() {
        return nitDocument;
    }

    public Sale nitDocument(String nitDocument) {
        this.nitDocument = nitDocument;
        return this;
    }

    public void setNitDocument(String nitDocument) {
        this.nitDocument = nitDocument;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public Sale total(BigDecimal total) {
        this.total = total;
        return this;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTotalPaid() {
        return totalPaid;
    }

    public Sale totalPaid(BigDecimal totalPaid) {
        this.totalPaid = totalPaid;
        return this;
    }

    public void setTotalPaid(BigDecimal totalPaid) {
        this.totalPaid = totalPaid;
    }

    public Set<ShoppingCartItem> getShoppingCartItems() {
        return shoppingCartItems;
    }

    public Sale shoppingCartItems(Set<ShoppingCartItem> shoppingCartItems) {
        this.shoppingCartItems = shoppingCartItems;
        return this;
    }

    public Sale addShoppingCartItem(ShoppingCartItem shoppingCartItem) {
        this.shoppingCartItems.add(shoppingCartItem);
        shoppingCartItem.setSale(this);
        return this;
    }

    public Sale removeShoppingCartItem(ShoppingCartItem shoppingCartItem) {
        this.shoppingCartItems.remove(shoppingCartItem);
        shoppingCartItem.setSale(null);
        return this;
    }

    public void setShoppingCartItems(Set<ShoppingCartItem> shoppingCartItems) {
        this.shoppingCartItems = shoppingCartItems;
    }

    public User getUser() {
        return user;
    }

    public Sale user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sale)) {
            return false;
        }
        return id != null && id.equals(((Sale) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Sale{" +
            "id=" + getId() +
            ", subtotal=" + getSubtotal() +
            ", productsQuantity=" + getProductsQuantity() +
            ", discount=" + getDiscount() +
            ", iva=" + getIva() +
            ", invoiceBusinessName='" + getInvoiceBusinessName() + "'" +
            ", nitDocument='" + getNitDocument() + "'" +
            ", total=" + getTotal() +
            ", totalPaid=" + getTotalPaid() +
            "}";
    }
}
