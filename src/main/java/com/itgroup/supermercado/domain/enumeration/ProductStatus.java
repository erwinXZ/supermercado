package com.itgroup.supermercado.domain.enumeration;

/**
 * The ProductStatus enumeration.
 */
public enum ProductStatus {
    AVAILABLE, NOT_AVAILABLE, NOT_PUBLISHED
}
